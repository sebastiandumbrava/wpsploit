#!/usr/bin/python3
import urllib3
from urllib.parse import urlparse
import urllib.request
import urllib.parse
import requests


# state information
_user_agent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0"
_accept_language = "en-US,en;q=0.5"
_accept_encoding = "gzip, deflate"
_referer = '' 
_cookies = '' 
_url = ''


def default_headers():
    global _user_agent, _accept_language, _accept_encoding, _connection
    default = dict()
    
    default['User-Agent'] = _user_agent
    default['Accept-Language'] = _accept_language
    default['Accept-Encoding'] = _accept_encoding
     
    return default


# first we get the cookies...
def login(url):
    global _cookies, _url
    #_cookies = "wordpress_test_cookie=WP Cookie check"
    parsed = urlparse(url)
    url = parsed.geturl()
    print(url)
    login_url = url + "/wp-login.php"
    login_headers = default_headers()
    #login_headers['Cookie'] = "wordpress_test_cookie=" + urllib.parse.quote_plus("WP Cookie check")
    req = urllib.request.Request(login_url, headers=login_headers, method="GET")
    response_prelogin = urllib.request.urlopen(req)
    cookie = response_prelogin.info().get_all('Set-Cookie')
    _cookies += cookie[0].split(';')[0]
    login_headers['Cookie'] = _cookies
    

    login_data = dict()
    login_data['log'] = 'elliot'
    login_data['pwd'] = 'ER28-0652'
    login_data['wp-submit'] = 'Log In'
    login_data['redirect_to'] = url + '/wp-admin/'
    login_data['testcookie'] = '1' 
    login_data_req = urllib.parse.urlencode(login_data).encode('ascii')
    req = urllib.request.Request(login_url, data=login_data_req, headers=login_headers, method='POST')
    #response = urllib.request.urlopen(req)
    response = requests.post(login_url,data=login_data,headers=login_headers)
    history = response.history
    print('-------------------')
    t = history[0].cookies
    print(type(t))
    print('-------------------')
    k = t.keys()
    v = t.values()
    d = dict()
    for i in range(len(k)):
        d[k[i]] = v[i]
    print('------------------------')
    h = response.cookies
    k = h.keys()
    v = h.values()
    for i in range(len(k)):
        d[k[i]] = v[i]
    #print(response.info().get_all('Set-Cookie'))
    
    _cookies = d
    _url = response.url


    return



def upload(filename):
    f = open(filename, "rb")
    file_data = f.read()
    f.close()
    #print(file_data)
    upload_url = "http://192.168.56.103/wp-admin/plugin-install.php?action=upload-plugin"
    upload_headers = default_headers()
    upload_headers['Cookie'] = urllib.parse.urlencode(_cookies)
     
    req = urllib.request.Request(upload_url, headers=upload_headers, method="POST")
    urllib.request.urlopen(req) 
    #response = requests.post(upload_url, data=upload_data, headers=upload_headers)
    return

def main():
    
    login("http://192.168.56.103")
    upload("plugin.zip")




    return


main()
