import http
import http.client

_client = None

def connection(host, port):
    global _client

    try:
        if (port == 80):
            _client = http.client.HTTPConnection(host, port, source_address=None)
        else:
            _client = http.client.HTTPSConnection(host, port)
    except:
        print("client cannot connect")

    return

def send_request(method, url, header={}):
    global _client
    _client.request(method, url, headers=header)
    return

def get_response():
    global _client
    tries = 0
    r = None

    while tries < 10000:
        try:
            r = _client.getresponse()
            break
        except:
            tries += 1

    return r

def set_tunnel(host, port):
    global _client

    _client.set_tunnel(host, port)
    return

def test():
    return
